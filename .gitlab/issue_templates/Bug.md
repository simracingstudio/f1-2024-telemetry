**Before opening a new issue**
* [ ] Search [existing issues](https://gitlab.com/gparent/f1-2024-telemetry/-/issues?scope=all) for keywords related to the problem
* [ ] Read the [documentation](https://f1-2024-telemetry.readthedocs.io/en/stable/)
* [ ] Test your issue with the latest master branch if possible

**Problem description**
<!-- What the bug is. Be precise. -->

**Steps to reproduce**
<!--
1. Run command 'buggy_command'
2. Get an error
-->

**Expected result**
<!-- What did you expect would happen instead? -->
