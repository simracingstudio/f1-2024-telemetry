"""
Various value enumerations used in the UDP output
"""
import enum
from typing import Dict


@enum.unique
class ButtonFlag(enum.IntEnum):
    """Bit-mask values for the 'button' field in Car Telemetry Data packets."""

    _ignore_ = "description"

    CROSS = 0x0001
    TRIANGLE = 0x0002
    CIRCLE = 0x0004
    SQUARE = 0x0008
    D_PAD_LEFT = 0x0010
    D_PAD_RIGHT = 0x0020
    D_PAD_UP = 0x0040
    D_PAD_DOWN = 0x0080
    OPTIONS = 0x0100
    L1 = 0x0200
    R1 = 0x0400
    L2 = 0x0800
    R2 = 0x1000
    LEFT_STICK_CLICK = 0x2000
    RIGHT_STICK_CLICK = 0x4000
    RIGHT_STICK_LEFT = 0x8000
    RIGHT_STICK_RIGHT = 0x10000
    RIGHT_STICK_UP = 0x20000

    description: Dict[enum.IntEnum, str]


ButtonFlag.description = {
    ButtonFlag.CROSS: "Cross or A",
    ButtonFlag.TRIANGLE: "Triangle or Y",
    ButtonFlag.CIRCLE: "Circle or B",
    ButtonFlag.SQUARE: "Square or X",
    ButtonFlag.D_PAD_LEFT: "D-pad Left",
    ButtonFlag.D_PAD_RIGHT: "D-pad Right",
    ButtonFlag.D_PAD_UP: "D-pad Up",
    ButtonFlag.D_PAD_DOWN: "D-pad Down",
    ButtonFlag.OPTIONS: "Options or Menu",
    ButtonFlag.L1: "L1 or LB",
    ButtonFlag.R1: "R1 or RB",
    ButtonFlag.L2: "L2 or LT",
    ButtonFlag.R2: "R2 or RT",
    ButtonFlag.LEFT_STICK_CLICK: "Left Stick Click",
    ButtonFlag.RIGHT_STICK_CLICK: "Right Stick Click",
    ButtonFlag.RIGHT_STICK_LEFT: "Right Stick Left",
    ButtonFlag.RIGHT_STICK_RIGHT: "Right Stick Right",
    ButtonFlag.RIGHT_STICK_UP: "Right Stick Up",
}

DriverIDs = {
    0: "Carlos Sainz",
    39: "Santiago Moreno",
    76: "Alain Prost",
    1: "Daniil Kvyat",
    40: "Benjamin Coppens",
    77: "Ayrton Senna",
    2: "Daniel Ricciardo",
    41: "Noah Visser",
    78: "Nobuharu Matsushita",
    3: "Fernando Alonso",
    42: "Gert Waldmuller",
    79: "Nikita Mazepin",
    4: "Felipe Massa",
    43: "Julian Quesada",
    80: "Guanya Zhou",
    6: "Kimi Räikkönen",
    44: "Daniel Jones",
    81: "Mick Schumacher",
    7: "Lewis Hamilton",
    45: "Artem Markelov",
    82: "Callum Ilott",
    9: "Max Verstappen",
    46: "Tadasuke Makino",
    83: "Juan Manuel Correa",
    10: "Nico Hulkenburg",
    47: "Sean Gelael",
    84: "Jordan King",
    11: "Kevin Magnussen",
    48: "Nyck De Vries",
    85: "Mahaveer Raghunathan",
    12: "Romain Grosjean",
    49: "Jack Aitken",
    86: "Tatiana Calderon",
    13: "Sebastian Vettel",
    50: "George Russell",
    87: "Anthoine Hubert",
    14: "Sergio Perez",
    51: "Maximilian Günther",
    88: "Guiliano Alesi",
    15: "Valtteri Bottas",
    52: "Nirei Fukuzumi",
    89: "Ralph Boschung",
    17: "Esteban Ocon",
    53: "Luca Ghiotto",
    90: "Michael Schumacher",
    19: "Lance Stroll",
    54: "Lando Norris",
    91: "Dan Ticktum",
    20: "Arron Barnes",
    55: "Sérgio Sette Câmara",
    92: "Marcus Armstrong",
    21: "Martin Giles",
    56: "Louis Delétraz",
    93: "Christian Lundgaard",
    22: "Alex Murray",
    57: "Antonio Fuoco",
    94: "Yuki Tsunoda",
    23: "Lucas Roth",
    58: "Charles Leclerc",
    95: "Jehan Daruvala",
    24: "Igor Correia",
    59: "Pierre Gasly",
    96: "Gulherme Samaia",
    25: "Sophie Levasseur",
    62: "Alexander Albon",
    97: "Pedro Piquet",
    26: "Jonas Schiffer",
    63: "Nicholas Latifi",
    98: "Felipe Drugovich",
    27: "Alain Forest",
    64: "Dorian Boccolacci",
    99: "Robert Schwartzman",
    28: "Jay Letourneau",
    65: "Niko Kari",
    100: "Roy Nissany",
    29: "Esto Saari",
    66: "Roberto Merhi",
    101: "Marino Sato",
    30: "Yasar Atiyeh",
    67: "Arjun Maini",
    102: "Aidan Jackson",
    31: "Callisto Calabresi",
    68: "Alessio Lorandi",
    103: "Casper Akkerman",
    32: "Naota Izum",
    69: "Ruben Meijer",
    109: "Jenson Button",
    33: "Howard Clarke",
    70: "Rashid Nair",
    110: "David Coulthard",
    34: "Wilheim Kaufmann",
    71: "Jack Tremblay",
    111: "Nico Rosberg",
    35: "Marie Laursen",
    72: "Devon Butler",
    36: "Flavio Nieves",
    37: "Peter Belousov",
    38: "Klimek Michalski",
    73: "Lukas Weber",
    74: "Antonio Giovinazzi",
    75: "Robert Kubica",
    112: "Oscar Piastri",
    113: "Liam Lawson",
    114: "Juri Vips",

}

InfringementTypes = {
    0: "Blocking by slow driving",
    1: "Blocking by wrong way driving",
    2: "Reversing off the start line",
    3: "Big Collision",
    4: "Small Collision",
    5: "Collision failed to hand back position single",
    6: "Collision failed to hand back position multiple",
    7: "Corner cutting gained time",
    8: "Corner cutting overtake single",
    9: "Corner cutting overtake multiple",
    10: "Crossed pit exit lane",
    11: "Ignoring blue flags",
    12: "Ignoring yellow flags",
    13: "Ignoring drive through",
    14: "Too many drive throughs",
    15: "Drive through reminder serve within n laps",
    16: "Drive through reminder serve this lap",
    17: "Pit lane speeding",
    18: "Parked for too long",
    19: "Ignoring tyre regulations",
    20: "Too many penalties",
    21: "Multiple warnings",
    22: "Approaching disqualification",
    23: "Tyre regulations select single",
    24: "Tyre regulations select multiple",
    25: "Lap invalidated corner cutting",
    26: "Lap invalidated running wide",
    27: "Corner cutting ran wide gained time minor",
    28: "Corner cutting ran wide gained time significant",
    29: "Corner cutting ran wide gained time extreme",
    30: "Lap invalidated wall riding",
    31: "Lap invalidated flashback used",
    32: "Lap invalidated reset to track",
    33: "Blocking the pitlane",
    34: "Jump start",
    35: "Safety car to car collision",
    36: "Safety car illegal overtake",
    37: "Safety car exceeding allowed pace",
    38: "Virtual safety car exceeding allowed pace",
    39: "Formation lap below allowed speed",
    40: "Retired mechanical failure",
    41: "Retired terminally damaged",
    42: "Safety car falling too far back",
    43: "Black flag timer",
    44: "Unserved stop go penalty",
    45: "Unserved drive through penalty",
    46: "Engine component change",
    47: "Gearbox change",
    48: "League grid penalty",
    49: "Retry penalty",
    50: "Illegal time gain",
    51: "Retry penalty",
    52: "Illegal time gain",
    53: "Mandatory pitstop",
    54: "Attribute assigned",




}

NationalityIDs = {
    1: "American",
    2: "Argentinian",
    3: "Australian",
    4: "Austrian",
    5: "Azerbaijani",
    6: "Bahraini",
    7: "Belgian",
    8: "Bolivian",
    9: "Brazilian",
    10: "British",
    11: "Bulgarian",
    12: "Cameroonian",
    13: "Canadian",
    14: "Chilean",
    15: "Chinese",
    16: "Colombian",
    17: "Costa Rican",
    18: "Croatian",
    19: "Cypriot",
    20: "Czech",
    21: "Danish",
    22: "Dutch",
    23: "Ecuadorian",
    24: "English",
    25: "Emirian",
    26: "Estonian",
    27: "Finnish",
    28: "French",
    29: "German",
    30: "Ghanaian",
    31: "Greek",
    32: "Guatemalan",
    33: "Honduran",
    34: "Hong Konger",
    35: "Hungarian",
    36: "Icelander",
    37: "Indian",
    38: "Indonesian",
    39: "Irish",
    40: "Israeli",
    41: "Italian",
    42: "Jamaican",
    43: "Japanese",
    44: "Jordanian",
    45: "Kuwaiti",
    46: "Latvian",
    47: "Lebanese",
    48: "Lithuanian",
    49: "Luxembourger",
    50: "Malaysian",
    51: "Maltese",
    52: "Mexican",
    53: "Monegasque",
    54: "New Zealander",
    55: "Nicaraguan",
    56: "Northern Irish",
    57: "Norwegian",
    58: "Omani",
    59: "Pakistani",
    60: "Panamanian",
    61: "Paraguayan",
    62: "Peruvian",
    63: "Polish",
    64: "Portuguese",
    65: "Qatari",
    66: "Romanian",
    67: "Russian",
    68: "Salvadoran",
    69: "Saudi",
    70: "Scottish",
    71: "Serbian",
    72: "Singaporean",
    73: "Slovakian",
    74: "Slovenian",
    75: "South Korean",
    76: "South African",
    77: "Spanish",
    78: "Swedish",
    79: "Swiss",
    80: "Thai",
    81: "Turkish",
    82: "Uruguayan",
    83: "Ukrainian",
    84: "Venezuelan",
    85: "Barbadian",
    86: "Welsh",
    87: "Vietnamese",
}

PenaltyTypes = {
    0: "Drive through",
    1: "Stop Go",
    2: "Grid penalty",
    3: "Penalty reminder",
    4: "Time penalty",
    5: "Warning",
    6: "Disqualified",
    7: "Removed from formation lap",
    8: "Parked too long timer",
    9: "Tyre regulations",
    10: "This lap invalidated",
    11: "This and next lap invalidated",
    12: "This lap invalidated without reason",
    13: "This and next lap invalidated without reason",
    14: "This and previous lap invalidated",
    15: "This and previous lap invalidated without reason",
    16: "Retired",
    17: "Black flag timer",
}

# These surface types are from physics data and show what type of contact each wheel is experiencing.
SurfaceTypes = {
    0: "Tarmac",
    1: "Rumble strip",
    2: "Concrete",
    3: "Rock",
    4: "Gravel",
    5: "Mud",
    6: "Sand",
    7: "Grass",
    8: "Water",
    9: "Cobblestone",
    10: "Metal",
    11: "Ridged",
}

TeamIDs = {
    0: "Mercedes",
    1: "Ferrari",
    2: "Red Bull Racing",
    3: "Williams",
    4: "Aston Martin",
    5: "Alpine",
    6: "RB",
    7: "Haas",
    8: "McLaren",
    9: "Sauber",

    10: "Unknown",
    11: "Unknown",
    12: "Unknown",
    13: "Unknown",
    14: "Unknown",
    15: "Unknown",
    16: "Unknown",
    17: "Unknown",
    18: "Unknown",
    19: "Unknown",

    20: "Unknown",
    21: "Unknown",
    22: "Unknown",
    23: "Unknown",
    24: "Unknown",
    25: "Unknown",
    26: "Unknown",
    27: "Unknown",
    28: "Unknown",
    29: "Unknown",

    30: "Unknown",
    31: "Unknown",
    32: "Unknown",
    33: "Unknown",
    34: "Unknown",
    35: "Unknown",
    36: "Unknown",
    37: "Unknown",
    38: "Unknown",
    39: "Unknown",

    40: "Unknown",
    41: "F1 Generic",
    42: "Unknown",
    43: "Unknown",
    44: "Unknown",
    45: "Unknown",
    46: "Unknown",
    47: "Unknown",
    48: "Unknown",
    49: "Unknown",

    50: "Unknown",
    51: "Unknown",
    52: "Unknown",
    53: "Unknown",
    54: "Unknown",
    55: "Unknown",
    56: "Unknown",
    57: "Unknown",
    58: "Unknown",
    59: "Unknown",

    60: "Unknown",
    61: "Unknown",
    62: "Unknown",
    63: "Unknown",
    64: "Unknown",
    65: "Unknown",
    66: "Unknown",
    67: "Unknown",
    68: "Unknown",
    69: "Unknown",

    70: "Unknown",
    71: "Unknown",
    72: "Unknown",
    73: "Unknown",
    74: "Unknown",
    75: "Unknown",
    76: "Unknown",
    77: "Unknown",
    78: "Unknown",
    79: "Unknown",

    80: "Unknown",
    81: "Unknown",
    82: "Unknown",
    83: "Unknown",
    84: "Unknown",

    85: "Unknown",
    86: "Unknown",
    87: "Unknown",
    88: "Unknown",
    89: "Unknown",
    90: "Unknown",
    91: "Unknown",
    92: "Unknown",
    93: "Unknown",
    94: "Unknown",
    95: "Unknown",
    96: "Unknown",
    97: "Unknown",
    98: "Unknown",
    99: "Unknown",
    100: "Unknown",
    101: "Unknown",
    102: "Unknown",
    103: "Unknown",
    104: "F1 Custom Team",
    106: "Unknown",
    107: "Unknown",
    108: "Unknown",
    109: "Unknown",
    110: "Unknown",
    111: "Unknown",
    112: "Unknown",
    113: "Unknown",
    114: "Unknown",
    115: "Unknown",
    116: "Unknown",
    117: "Unknown",
    118: "Unknown",
    119: "Unknown",
    120: "Unknown",
    121: "Unknown",
    122: "Unknown",
    123: "Unknown",
    124: "Unknown",
    125: "Unknown",
    126: "Unknown",
    127: "Unknown",
    128: "Unknown",
    129: "Unknown",
    130: "Unknown",
    131: "Unknown",
    132: "Unknown",
    133: "Unknown",
    134: "Unknown",
    135: "Unknown",
    136: "Unknown",
    137: "Unknown",
    138: "Unknown",
    139: "Unknown",
    140: "Unknown",
    141: "Unknown",
    142: "Unknown",
    143: "Art GP ‘23",
    144: "Campos ‘23",
    145: "Carlin ‘23",
    146: "PHM ‘23",
    147: "Dams ‘23",
    148: "Hitech ‘23",
    149: "MP Motorsport ‘23",
    150: "Prema ‘23",
    151: "Trident ‘23",
    152: "Van Amersfoort Racing ‘23",
    153: "Virtuosi ‘23",
    154: "Unknown",
    155: "Unknown",
    156: "Unknown",
    157: "Unknown",
    158: "Unknown",
    159: "Unknown",
    160: "Unknown",
    161: "Unknown",
    162: "Unknown",
    163: "Unknown",
    164: "Unknown",
    165: "Unknown",
    166: "Unknown",
    167: "Unknown",
    168: "Unknown",
    169: "Unknown",
    170: "Unknown",
    171: "Unknown",
    172: "Unknown",
    173: "Unknown",
    174: "Unknown",
    175: "Unknown",
    176: "Unknown",
    177: "Unknown",
    178: "Unknown",
    179: "Unknown",
    180: "Unknown",
    181: "Unknown",
    182: "Unknown",
    183: "Unknown",
    184: "Unknown",
    185: "Unknown",
    186: "Unknown",
    187: "Unknown",
    188: "Unknown",
    189: "Unknown",
    190: "Unknown",
    191: "Unknown",
    192: "Unknown",
    193: "Unknown",
    194: "Unknown",
    195: "Unknown",
    196: "Unknown",
    197: "Unknown",
    198: "Unknown",
    199: "Unknown",
    200: "Unknown",
    201: "Unknown",
    202: "Unknown",
    203: "Unknown",
    204: "Unknown",
    205: "Unknown",
    206: "Unknown",
    207: "Unknown",
    208: "Unknown",
    209: "Unknown",
    210: "Unknown",
    211: "Unknown",
    212: "Unknown",
    213: "Unknown",
    214: "Unknown",
    215: "Unknown",
    216: "Unknown",
    217: "Unknown",
    218: "Unknown",
    219: "Unknown",
    220: "Unknown",
    221: "Unknown",
    222: "Unknown",
    223: "Unknown",
    224: "Unknown",
    225: "Unknown",
    226: "Unknown",
    227: "Unknown",
    228: "Unknown",
    229: "Unknown",
    230: "Unknown",
    231: "Unknown",
    232: "Unknown",
    233: "Unknown",
    234: "Unknown",
    235: "Unknown",
    236: "Unknown",
    237: "Unknown",
    238: "Unknown",
    239: "Unknown",
    240: "Unknown",
    241: "Unknown",
    242: "Unknown",
    243: "Unknown",
    244: "Unknown",
    245: "Unknown",
    246: "Unknown",
    247: "Unknown",
    248: "Unknown",
    249: "Unknown",
    250: "Unknown",
    251: "Unknown",
    252: "Unknown",
    253: "Unknown",
    254: "Unknown",
    255: "My Team",
}

TrackIDs = {
    0: "Melbourne",
    1: "Paul Ricard",
    2: "Shanghai",
    3: "Sakhir (Bahrain)",
    4: "Catalunya",
    5: "Monaco",
    6: "Montreal",
    7: "Silverstone",
    8: "Hockenheim",
    9: "Hungaroring",
    10: "Spa",
    11: "Monza",
    12: "Singapore",
    13: "Suzuka",
    14: "Abu Dhabi",
    15: "Texas",
    16: "Brazil",
    17: "Austria",
    18: "Sochi",
    19: "Mexico",
    20: "Baku (Azerbaijan)",
    21: "Sakhir Short",
    22: "Silverstone Short",
    23: "Texas Short",
    24: "Suzuka Short",
    25: "Hanoi",
    26: "Zandvoort",
    27: "Imola",
    28: "Portimão",
    29: "Jeddah",
    30: "Miami",
    31: "Las Vegas",
    32: "Losail",

}
