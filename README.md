
F1 2024 UDP Telemetry package
=============================

The f1-2024-telemetry package provides support for interpreting telemetry information as sent out over the network by [the F1 2021 game by CodeMasters](https://www.ea.com/games/f1/f1-2024).
It also provides command-line tools to record, playback, and monitor F1 2024 session data.

With each yearly release of the F1 series game, CodeMasters post a description of the corresponding telemetry packet format on their forum.
For F1 2024, the packet format is described here: https://forums.codemasters.com/topic/80231-f1-2024-udp-specification/?tab=comments

The package should work on Python 3.6 and above.

This project is a fork of f1-2020-telemetry by gparent for the F12020
that is also a fork of f1-2019-telemetry by Sidney Cadot (reddish) for F1 2019


Project information
-------------------

The f1-2024-telemetry package and its documentation are currently at version **0.0.1**.

The project can be installed using the standard Python 3 _pip_ tool as follows:

    pip install f1-2024-telemetry

The project is hosted as a Git repository on [GitLab](https://gitlab.com):

  https://gitlab.com/simracingstudio/f1-2024-telemetry/

The pip-installable package is hosted on [PyPI](https://pypi.org):

  https://pypi.org/project/f1-2024-telemetry/

The documentation is hosted on [Read the Docs](https://readthedocs.org):

  https://f1-2024-telemetry.readthedocs.io/en/latest/
